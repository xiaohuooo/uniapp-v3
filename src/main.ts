import '~/styles/index.scss'
import { createSSRApp } from 'vue'
import App from './App.vue'
// #ifvar-dev
console.log('我是开发模式')
// #endvar
// #ifvar-pro
console.log('我是线上模式')
// #endvar
export function createApp() {
    const app = createSSRApp(App)
    return {
        app
    }
}
