import '@vue/runtime-core'
    export {}
    declare module '@vue/runtime-core' {
        export interface GlobalComponents {

        sDialog:typeof import('./dialog/index.vue')['default']
        sHeader:typeof import('./header/index.vue')['default']
        sIcon:typeof import('./icon/index.vue')['default']
        sLine:typeof import('./line/index.vue')['default']
        sLogin:typeof import('./login/index.vue')['default']
        sTabbar:typeof import('./tabbar/index.vue')['default']
        sTheme:typeof import('./theme/index.vue')['default']

        }
    }
    