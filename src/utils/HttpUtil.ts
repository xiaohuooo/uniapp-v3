import { HttpBean } from 'tools-uniapp'
export default class HttpUtil {
    static http: HttpInter = null as any

    static post<T = any>(optoins: any, data: any = {}) {
        if (this.http == null) this.init()
        return this.http.post<T>(optoins, data)
    }

    static get<T = any>(optoins: any, data: any = {}) {
        if (this.http == null) this.init()
        return this.http.get<T>(optoins, data)
    }

    static async init() {
        let url = 'http://c44.cc'

        // #ifvar-dev
        console.log('我是开发模式')
        url = 'http://192.168.1.12:8101'
        // #endvar

        // #ifvar-pro
        console.log('我是线上模式')

        //#ifdef APP-PLUS
        url = 'http://pro-app-plus.c44.cc'
        //#endif

        //#ifdef H5 || MP-WEIXIN
        url = 'http://pro-app-h5.c44.cc'
        //#endif

        // #endvar
        console.log('httpurl', url)

        this.http = HttpBean({
            base: url,
            before: (config) => {
                config.headers['Authorization'] = 'Bearer '
            },
            after: (xhr, config) => {},
            error: () => {}
        })
    }
}
