import { reactive } from 'vue'

const CIndex = reactive({
    active: '首页',
    toIndex: (name: string = '首页') => {
        CIndex.active = name
        uni.reLaunch({ url: '/pages/index/index' })
    }
})
export default CIndex
