import { CookieUtil } from 'tools-uniapp'
import { reactive } from 'vue'

const themeCache = CookieUtil.get('theme')
const themeList = ['light', 'dark']
const CTheme = reactive({
    theme: themeList.includes(themeCache) ? themeCache : 'light',
    setTheme: (name: string) => {
        if (themeList.includes(name)) {
            CTheme.theme = name
            CookieUtil.set('theme', name)
        }
    }
})
export default CTheme
