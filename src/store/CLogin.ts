import { reactive } from 'vue'

const CLogin = reactive({
    dialog: {
        show: false,
        content: '请先登录之后再进行操作'
    }
})
export default CLogin
