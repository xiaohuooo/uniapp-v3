import { defineConfig } from 'vite'
import * as path from 'path'
import uni from '@dcloudio/vite-plugin-uni'
import { siconPlugin } from 'sicon-plugin'
import { viteComType, viteVar, viteDef } from 'vite-var'
import env from './env'

// https://vitejs.dev/config/
export default defineConfig((config) => {
    const isPro = config.mode == 'production'
    return {
        resolve: {
            alias: {
                '~': path.join(__dirname, './src')
            }
        },
        plugins: [
            viteVar(env),
            viteDef(isPro ? 'pro' : 'dev'),
            viteComType(),
            siconPlugin({
                isBuild: true,
                comUrl: './src/components/icon',
                include: {
                    Ant: ['home', 'user', 'shopping', 'send']
                },
                tagName: 's-icon'
            }),
            uni()
        ],
        build: {
            minify: 'terser',
            terserOptions: {
                compress: {
                    keep_infinity: true,
                    drop_console: false,
                    drop_debugger: true
                }
            }
        }
    }
})
