# 🌈 uni-app Vue3 Vite4 TypeScript 小程序脚手架

## 简介

-   **uni-app Vue3 Vite4 TypeScript 基础框架**
-   request请求封装，websocket封装（支持多连接管理）
-   自定义头部导航，自定义底部tabbar
-   全局登录拦截，自定义登录提示
-   全局事件管理，父子组件成员共享，文件上传（服务端、七牛云）
-   支持主题切换，方便使用的css样式
-   iconfont图标支持按需加载按需打包、图标支持单色和多色控制、图标体积压缩
## 特性

-   **最新技术栈**：使用 Vue3/Vite4 ,TypeScript 等前端前沿技术开发;
-   **Prettier**: 规范代码格式,统一编码;
-   **iconfont**: [iconfont](https://www.iconfont.cn/),支持按需使用按需打包、单色和多色配置显示、开源库和私有库配置使用;

## 目录结构

```shell
.
├─ src
│   ├─static # 静态公共文件
│   │
│   ├─components # 组件目录
│   │   ├─ sicon
│   │   │    ├─index.vue
│   │   └─...
│   │
│   ├─pages # 页面
│   │   ├─ index
│   │   │    └─index.vue
│   │   └─...
│   │
│   ├─api # 接口相关
│   │   ├─ ApiUser
│   │   └─...
│   │
│   ├─store # 状态管理模式(reactive式管理)
│   │   ├─ CSystem
│   │   └─...
│   │
│   └─utils # 工具类
│       ├─ HttpUtil # request请求
│       ├─ CEvent # 全局事件管理
│       ├─ OssUtil  # 文件上传
│       ├─ Scope  # 父子组件成员共享
│       └─ ...
│
├─ .gitignore
├─ .prettierignore
├─ .prettierrc
├─ index.html
├─ package.json
├─ README.md
├─ tsconfig.json
└─ vite.config.ts

```

## 安装使用

-   安装依赖

```bash
pnpm install
```

-   运行

```bash
# 其他端请查看 package.json script
pnpm dev:h5
```

-   打包

```bash
# 其他端请查看 package.json script
pnpm build:h5
```

-   更新依赖到最新（新手请忽略）

```bash
pnpm up
# 打开HBuilder X alpha桌面程序-->点击上面的帮助-->历次更新说明-->获取最新版本号（如：3.7.2.20230217-alpha）
npx @dcloudio/uvm 3.7.2.20230217-alpha
```

## Gitee 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

### 提交类型

| 提交类型   | 标题               | 描述                                                                                  |
| ---------- | ------------------ | ------------------------------------------------------------------------------------- |
| `feat`     | 特征               | 新功能、新特性                                                                        |
| `fix`      | Bug 修复           | bug 修复                                                                              |
| `docs`     | 文档               | 仅文档更改                                                                            |
| `style`    | 风格               | 不影响代码含义的更改（空格、格式、缺少分号等）                                        |
| `refactor` | 代码重构           | 重构，在不影响代码内部行为，功能下的代码修改                                          |
| `perf`     | 性能改进           | 更改代码，以提高性能                                                                  |
| `test`     | 测试               | 添加缺失的测试或纠正现有的测试                                                        |
| `build`    | 构建               | 影响构建系统或外部依赖项的更改（示例范围：gulp、broccoli、npm）                       |
| `ci`       | 持续集成           | 对我们的 CI 配置文件和脚本的更改（示例范围：Travis、Circle、BrowserStack、SauceLabs） |
| `chore`    | 其他文件修改       | 不修改 src 或测试文件的其他更改                                                       |
| `revert`   | 还原               | 恢复之前的提交                                                                        |
| `release`  | 发布新版本         | \-                                                                                    |
| `workflow` | 工作流相关文件修改 | \-                                                                                    |

### 提交别名

| 提交类型           | 映射到  | 标题     | 描述                       |
| ------------------ | ------- | -------- | -------------------------- |
| `initial`          | `feat`  | 最初的   | 初始提交                   |
| `dependencies`     | `fix`   | 依赖项   | 更新依赖项                 |
| `peerDependencies` | `fix`   | 对等依赖 | 更新对等依赖项             |
| `devDependencies`  | `chore` | 开发依赖 | 更新开发依赖               |
| `metadata`         | `fix`   | 元数据   | 更新元数据（package.json） |

### 快捷别名提示

1.  resolve a conflict：解决冲突
2.  merge branch：合并分支
3.  feat: [...] : 添加的新功能说明
4.  fix: [...] : 修复的 bug 说明
5.  initial project：初始化项目
6.  style: [...] : 修改的样式范围
7.  perf：[...] : 优化的范围
8.  release : 发布新版本
9.  docs: 文档修改
10. refactor： 代码重构
11. revert： 还原之前的版本
12. dependencies： 依赖项修改
13. devDependencies： 开发依赖修改
14. review：复习，回顾
15. strengthen: 加强，巩固

## 
